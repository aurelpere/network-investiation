#! /usr/bin/python3
# -*- coding:Utf8 -*-
"""
connect to wifi with cli on debian
"""
#lister les cartes wifi installées sur la machine
import subprocess
import re
import os
from getpass import getpass
from time import sleep
print ('\nEtes vous connectez en filaire? o/n')
input_fil=input()

def search_fil_interface(file_):  
    with open(file_,"r") as fileo:
        active_fil_lines = [line.strip() for line in fileo if "" in line]##############checker TYPE dans nmcli connection show --active
    for fil_line in active_fil_lines:
        pattern="(?<=\s{4})\w+"##############checker TYPE dans nmcli connection show --active
        interface_re_search=re.search(pattern,fil_line)
        if interface_re_search:
            print(f"votre interface filaire est bien {interface_re_search.group(0)}? o/n")
            answer=input()
            if answer=='o' or answer=='O' or answer=='y' or answer=='Y':
                return fil_interface=interface_re_search.group(0)

if input_fil=='o' or input_fil=='O' or input_fil=='y' or input_fil=='Y':
    print('souhaitez vous scanner le réseau pour récupérer netmask, broadcast, gateway, et votre ip? o/n')
    input_scan=input()
        if input_scan=='o' or input_scan=='O' or input_scan=='y' or input_scan=='Y':
            cmd="nmcli connection show --active > active.txt"           
            child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            print(child.stdout.decode('utf-8'))
            print(child.stderr.decode('utf-8'))
            fil_interface=search_fil_interface('active.txt')

def return_ip_brd_nm(interface):
    result_list=[]
    cmd="ifconfig > ifconfig.txt"           
    child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print(child.stdout.decode('utf-8'))
    print(child.stderr.decode('utf-8'))
    with open('ifconfig.txt',"r") as fileo:
        scan_lines_list = [line.strip('\n') for line in fileo]    
        for idx,line in enumerate(scan_lines_list):
            if f'{interface}' in line:
                line1=scan_lines_list[idx]
                print(line1)
                line2=scan_lines_list[idx+1]
                ip_pattern="(?<=inet\s)\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"
                ip_re_search=re.search(ip_pattern,line2)
                if ip_re_search:
                    result_list.append(ip_re_search.group(0))
                netmask_pattern="(?<=netmask\s)\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"
                netmask_re_search=re.search(netmask_pattern,line2)
                if netmask_re_search:
                    result_list.append(netmask_re_search.group(0))
                brd_pattern="(?<=broadcast\s)\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"
                brd_re_search=re.search(brd_pattern,line2)
                if brd_re_search:
                    result_list.append(brd_re_search.group(0))
                print(line2)
                #guessing gateway before improvement
                gw_pattern="(?<=inet\s)\d{1,3}\.\d{1,3}\.\d{1,3}"
                gw_re_search=re.search(gw_pattern,line2)
                if gw_re_search:
                    result_list.append(f'{gw_re_search.group(0)}.1)     
    with open (ip_nm_brd_gw.txt) as fileo:
        for item in result_list:
            fileo.write(item)
            fileo.write('\n')
    return result_list#[ip,netmask,broadcast,gateway]

if input_scan=='o' or input_scan=='O' or input_scan=='y' or input_scan=='Y':
    ip_nm_brd_gw=return_ip_brd_nm(fil_interface)


print ('\nlisting des cartes wifi installées sur la machine')
print ('---------------------------------------------------------------------')
cmd="iwconfig > wifi_cards.txt"
child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
#print(child.stdout.decode('utf-8'))
#print(child.stderr.decode('utf-8'))
with open("wifi_cards.txt","r") as fileo:
    iwconfig_ieee_lines = [line.strip() for line in fileo if "IEEE" in line]
for wifi_line in iwconfig_ieee_lines:
    pattern="\w+(?=\s+IEEE)"
    interface_re_search=re.search(pattern,wifi_line)
    if interface_re_search:
        print(f"votre interface wifi est bien {interface_re_search.group(0)}? o/n")
        answer=input()
        if answer=='o' or answer=='O' or answer=='y' or answer=='Y':
            interface=interface_re_search.group(0)
            break
        else:
            pass
print('\nquel ESSID voulez vous scannez pour récupérer les BSSID?')
input_essid=input()

print ('\nscan des reseaux')
print ('---------------------------------------------------------------------')
cmd=f"sudo iwlist {interface} scan > scan.txt"
child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
print(child.stdout.decode('utf-8'))
print(child.stderr.decode('utf-8'))

def return_bssid(file_):
    result_list=[]
    with open(file_,"r") as fileo:
        scan_lines_list = [line.strip('\n') for line in fileo]    
        for idx,line in enumerate(scan_lines_list):
            if f'ESSID:"{input_essid}"' in line:
                line1=scan_lines_list[idx]
                print(line1)
                line2=scan_lines_list[idx-5]
                pattern="(?<=Address:\s)\w{2}:\w{2}:\w{2}:\w{2}:\w{2}:\w{2}"
                bssid_re_search=re.search(pattern,line2)
                if bssid_re_search:
                    result_list.append(bssid_re_search.group(0))
                print(line2)
                line3=scan_lines_list[idx-4]
                print(line3)
                for subline in [scan_lines_list[i] for i in range(idx+1,idx+20)]:
                    if "IEEE" in subline:
                        line4=subline
                        print(line4)              
    return result_list
bssid_list=resutrn_bssid('scan.txt')
print("Entrer l'ip statique que vous souhaitez sur ce réseau 192.168.1.x'")
ip=input()
#mettre un re check ?


print("Entrer votre mot de passe pour ce réseau (8 caracteres ou plus)")
password = getpass()

print ('Adding Connection WF3-Ecole without proxy')
#ajouter une boucle for sur les bssid si necessaire
txt=f"""
[connection]
id=WF3-Ecole
uuid=f153e00a-bf79-3425-95e4-81801ed7acf1
type=wifi
autoconnect-priority=1
interface-name=wlp3s0
permissions=

[wifi]
bssid=A2:46:9D:1A:59:08
mac-address-blacklist=
mode=infrastructure
seen-bssids="""
txt+=';'.join(return_bssid("scan.txt"))
txt+='\n'
txt2=f"""
ssid=WF3-Ecole

[wifi-security]
key-mgmt=wpa-psk
psk={password}

[ipv4]
address1={ip}/24,{ip_nm_brd_gw[3]}
dns-search=
may-fail=false
method=manual

[ipv6]
addr-gen-mode=stable-privacy
dns-search=
method=ignore"""
txt+=txt2
txt3="""
[proxy]
method=1
pac-url=10.11.0.1"""
txt+=txt3
with open('connection','w',encoding='utf-8') as fileo:
fileo.write(txt)
#man nm-settings-keyfile
cmd_list=['sudo chmod 0600 connection',
          'sudo chown root:root connection',
for cmd in cmd_list:        
    child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print(child.stdout.decode('utf-8'))
    print(child.stderr.decode('utf-8'))



for bssid_i in return_bssid("scan.txt"):#il faudrait trouver une condition "succès "sur la commande sudo dhclient -v {interface}(la derniere) pr faire la boucle correctement
    print (f'\nEssai de connection sur le bssid {bssid_i}')
    print ('---------------------------------------------------------------------')
    cmd=f'sudo pkill wpa_supplicant'
    child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print(child.stdout.decode('utf-8'))
    print(child.stderr.decode('utf-8'))
    
    cmd=f'sudo systemctl stop NetworkManager'
    child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print(child.stdout.decode('utf-8'))
    print(child.stderr.decode('utf-8'))
    
    cmd=f'sudo ip link set {interface} down'
    child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print(child.stdout.decode('utf-8'))
    print(child.stderr.decode('utf-8'))
    
    cmd=f'sudo ip addr flush dev {interface}'
    child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print(child.stdout.decode('utf-8'))
    print(child.stderr.decode('utf-8'))
    
    cmd=f'sudo ip link set {interface} up'
    child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print(child.stdout.decode('utf-8'))
    print(child.stderr.decode('utf-8'))
    
    cmd=f'nmcli con up id "WF3-Ecole"'
    child=subprocess.run(cmd, shell=True,check=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    print(child.stdout.decode('utf-8'))
    print(child.stderr.decode('utf-8'))
    


    
#os.remove('scan.txt')
#os.remove('wifi_cards.txt')
#os.remove('wpa.conf')
#os.remove('active.txt')
#os.remove('ifconfig.txt')

