## commands to check and tune wifi


###### infos about all interfaces
`ifconfig -a or ip a`

###### list hardware and virtual interfaces
`ls /sys/class/net`

###### activate/desactivate <interface>
`sudo ifup/ifdown <interface>`

###### activate/desactivate <interface>
`ip link set <interface> up/down`

###### delete interface
`ip link delete <interface> up/down`

###### block or unblock wifi
`sudo rfkill block/unblock wifi`

###### block or unblock <interface>
`sudo rfkill block/unblock <interface>`

###### show available wifi networks
`nmcli wifi list`

###### connect to bssid
`nmcli wifi connect (B)SSID password pw ifname wlp3s0 private yes`

###### add a connection with a type
`nmcli connection add connection.type 'team/bridge/wifi' con-name 'team' ifname 'wlp3s0' ipv4.method 'shared' ipv4.addresses '192.168.2.1/24`

###### create nmcli password file
`echo "802-11-wireless-security.psk:monmotdepasse" >> pw`

###### connect to access point
`nmcli connection up ifname 'wlp3s0' passwd-file 'pw' ap 'A4:3E:51:55:C5:20'`
`nmcli con up f153e00a-bf79-3425-95e4-81801ed7acf1 ap A4:3E:51:55:C5:20`
`nmcli con up id "Connection id name"`


## scan de réseau 

###### scan <ip> network
`nmap -sP <ip>`

###### currently logged users
`w`

###### Each line in this file represents login information for one user who can use the shell
`cat /etc/passwd | grep /bin/bash`

###### The last command lists the sessions of users who recently logged in to the system
`last`

###### failed login
`lastb (fail)`


`cat /var/log/messages`

###### ssh connections attempts 
`sudo cat /var/log/auth.log | grep ssh | grep Accept`

###### show bash history
`history`

###### check the files, their owner and time of modification
`ls -lart`

###### displays the current listening sockets on the machine
`netstat -na` 

###### Looks for any suspicious connections that are running on odd ports
`netstat -natp`

###### track down any errant processes that are listening and shows other odd processes 
`ps -wauxef`

###### to find more information about open files that a process is using
`lsof |grep <pid>`

######might also tell you where the file that controls a process exists.
`cat /proc/<pid>/cmdline`


###### check cpu intensive processes
`top`

###### check immutability ex: -------i----- /bin/ps
`lsattr in /bin /sbin /usr/bin /usr/sbin`

###### who is the author of the file
`ls -la –author`

###### find files modified in the past 5 days : find / -mtime 5
`find`


## check rootkit
`apt install rkhunter
rkhunter --check
/var/log/rkhunter.log`

## trivy for vuln check or iac misconfiguration
`trivy repo : https://github.com/knqyf263/trivy-ci-test
apt-get install trivy`

#### vulnerability
`trivy rootfs --severity HIGH,CRITICAL /
trivy fs --severity HIGH,CRITICAL path`

#### Iac misconfiguration 
`trivy config --severity HIGH,CRITICAL path
trivy image --severity HIGH,CRITICAL --security-checks config IMAGE_NAME #docker image
trivy fs --severity HIGH,CRITICAL --security-checks config /path/to/dir #path`
